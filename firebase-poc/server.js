const express = require('express');
const app = express();
const routes = require('./apiRoutes');
const config = require('./config');
const firebase = require("firebase-admin");
const serviceAccount = require("./app-manag.json.js");
const port = process.env.PORT || 8080;

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    storageBucket: config.firebaseStorageBucketURL
});
const bucket = firebase.storage().bucket();

app.use(function (req, res, next) {
    if (!req.admin) {
        req.admin = firebase;
    }
    if (!req.bucket) {
        req.bucket = bucket;
    }
    next();
});
app.get('/', (req, res) => res.send('App is working'))
app.use('/api', routes)
app.listen(port);

console.log('Firebase Listening on the port ' + port);

module.exports = {
    app
}