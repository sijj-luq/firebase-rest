const firebase = require("firebase-admin");
const serviceAccount = require("./app-manag.json");

firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: "https://app-management-5585d.firebaseio.com"
});

var db = firebase.database();
var ref = db.ref("restricted_access/secret_document");
ref.once("value", function(snapshot) {
  console.log(snapshot.val());
});

var usersRef = ref.child("users");
usersRef.set({
  SirajBase: {
    date_of_birth: "JAn 28, 1991",
    full_name: "Sirajuddeen"
  },
  ShaheenBase: {
    date_of_birth: "May 1, 1989",
    full_name: "shaheen soghra fathima"
  }
});