const express = require('express');
const controller = require('./modelController')
const router = express.Router();
const Multer = require('multer');

const multer = Multer({
    storage: Multer.memoryStorage(),
    limits: {
        fileSize: 5 * 1024 * 1024
    }
});
router.get('/getDetails', controller.getDetails)

router.post('/uploadImage',multer.single("fireImage"), controller.uploadImage)

module.exports = router
 